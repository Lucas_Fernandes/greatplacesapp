import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqlite_api.dart';

class DbHelper {
  static Future<Database> _database() async {
    //CREATING THE DATABASE
    final dbPath = await sql
        .getDatabasesPath(); // RETURNS THE PLACE WHERE THE DB IS STORED.
    return await sql.openDatabase(
      path.join(dbPath, "places.db"),
      onCreate: (db, version) {
        // CREATES THE DB (BASED ON THE PATH) IF IS NOT CREATED ALREADY.
        return db.execute(
          "CREATE TABLE user_places(id TEXT PRIMARY KEY,"
              "title TEXT,"
              "image TEXT,"
              "loc_lat REAL,"
              "loc_lng REAL,"
              "address TEXT)",
        );
      },
      version: 2,
    );
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    //GETTING THE DATABASE
    final Database db = await _database();

    //IF THE DB IS ALREADY CREATED, IT WILL JUST INSERT THE DATA
    db.insert(
      table,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace, // OVERRIDES AN EXISTING RECORD WITH A MATCHING ID
    );
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    //GETTING THE DATABASE
    final Database db = await _database();
    return db.query(table);
  }
}
