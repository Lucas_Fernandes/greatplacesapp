import 'package:http/http.dart' as http;
import 'dart:convert';

const GOOGLE_API_KEY = "AIzaSyBu8BcaI05hf5vBDSW7dlofFh6nEZEOnJg";

class LocationHelper {

  /// RETURNS THE API URL THAT GENERATES A GOOGLE MAPS IMAGE GIVEN A
  /// latitude AND longitude.
  static String generateLocationPreviewImage({
    required double latitude,
    required double longitude,
  }) {
    return "https://maps.googleapis.com/maps/api/staticmap?center=$latitude,$longitude"
        "&zoom=16&size=600x300&maptype=roadmap"
        "&markers=color:red%7Clabel:A%7C$latitude,$longitude"
        "&key=$GOOGLE_API_KEY";
  }

  /// TURNS THE latitude AND longitude INTO A HUMAN READABLE ADDRESS.
  static Future<String> getPlaceAddress({
    required double lat,
    required double lng,
  }) async {
    final url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,"
        "$lng&key=$GOOGLE_API_KEY";
    final response = await http.get(Uri.parse(url));
    return json.decode(response.body)["results"][0]["formatted_address"];
  }
}
