import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_places_app/helpers/location_helper.dart';
import 'package:great_places_app/models/place_location.dart';
import 'package:great_places_app/screens/maps_screen.dart';
import 'package:location/location.dart';

class LocationInput extends StatefulWidget {
  final Function selectPlace;

  const LocationInput({
    Key? key,
    required this.selectPlace,
  }) : super(key: key);

  @override
  State<LocationInput> createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String? _previewImageUrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 170,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Colors.grey,
            ),
          ),
          child: _previewImageUrl == null
              ? const Text(
                  "No location chosen",
                  textAlign: TextAlign.center,
                )
              : Image.network(
                  _previewImageUrl!,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton.icon(
              icon: const Icon(Icons.location_on),
              label: const Text("Current Location"),
              style: TextButton.styleFrom(
                textStyle: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: _getCurrentUserLocation,
            ),
            TextButton.icon(
              icon: const Icon(Icons.map),
              label: const Text("Select on Map"),
              style: TextButton.styleFrom(
                textStyle: TextStyle(color: Theme.of(context).primaryColor),
              ),
              onPressed: _selectOnMap,
            ),
          ],
        )
      ],
    );
  }

  Future<void> _getCurrentUserLocation() async {
    try {
      LocationData locData = await getLocationData();
      if (locData.latitude != null && locData.longitude != null) {
        _showPreview(locData.latitude!, locData.longitude!);
        widget.selectPlace(locData.latitude, locData.longitude);
      }
    } catch (error) {
      return;
    }
  }

  Future<void> _selectOnMap() async {
    try {
      LocationData locData = await getLocationData();
      if (locData.latitude == null && locData.longitude == null) {
        return;
      }

      // NAVIGATING TO A NEW SCREEN WITHOUT REGISTERING THE ROUTE IN THE TABLE.
      // ALSO GETTING THE SELECTED LOCATION FROM THE MapsScreen.
      final selectedLocation = await Navigator.of(context).push<LatLng>(
        MaterialPageRoute(
          fullscreenDialog: true,
          builder: (ctx) => MapsScreen(
            initialLocation: PlaceLocation(
              latitude: locData.latitude ?? 0.0,
              longitude: locData.longitude ?? 0.0,
            ),
            isSelecting: true,
          ),
        ),
      );

      if (selectedLocation == null) {
        return;
      }

      _showPreview(selectedLocation.latitude, selectedLocation.longitude);

      widget.selectPlace(
        selectedLocation.latitude,
        selectedLocation.longitude,
      );
    } catch (error) {
      return;
    }
  }

  Future<LocationData> getLocationData() async {
    return Location().getLocation();
  }

  void _showPreview(double latitude, double longitude) {
    final staticMapImageUrl = LocationHelper.generateLocationPreviewImage(
      latitude: latitude,
      longitude: longitude,
    );

    setState(() {
      _previewImageUrl = staticMapImageUrl;
    });
  }
}
