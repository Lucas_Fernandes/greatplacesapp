import 'dart:io';

import 'package:great_places_app/models/place_location.dart';

class Place {
  final String id;
  final String title;
  final PlaceLocation location;

  // File data type is provided by Dart to work with the file system.
  final File image;

  Place({
    required this.id,
    required this.title,
    required this.location,
    required this.image,
  });
}
