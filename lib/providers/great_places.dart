import 'dart:io';

import 'package:flutter/material.dart';
import 'package:great_places_app/helpers/db_helper.dart';
import 'package:great_places_app/helpers/location_helper.dart';
import 'package:great_places_app/models/place.dart';
import 'package:great_places_app/models/place_location.dart';

class GreatPlaces with ChangeNotifier {
  static const _tableName = "user_places";
  List<Place> _items = [];

  List<Place> get items {
    return [..._items];
  }

  /// CREATES A NEW PLACE
  //CREATING A NEW PLACE
  Future<void> addPlace(
    String pickedTitle,
    File pickedImage,
    PlaceLocation pickedLocation,
  ) async {

    //GETTING THE HUMAN READABLE ADDRESS.
    final readableAddress = await LocationHelper.getPlaceAddress(
      lat: pickedLocation.latitude,
      lng: pickedLocation.longitude,
    );

    final updatedLocation = PlaceLocation(
      latitude: pickedLocation.latitude,
      longitude: pickedLocation.longitude,
      address: readableAddress,
    );

    final newPlace = Place(
      id: DateTime.now().toString(),
      title: pickedTitle,
      image: pickedImage,
      location: updatedLocation,
    );

    _items.add(newPlace);
    notifyListeners();

    //STORING THE OBJECT IN THE DB.
    DbHelper.insert(_tableName, {
      "id": newPlace.id,
      "title": newPlace.title,
      "image": newPlace.image.path,
      "loc_lat": newPlace.location.latitude,
      "loc_lng": newPlace.location.longitude,
      "address": newPlace.location.address!,
    });
  }

  /// RETURNS ALL THE PLACES STORED IN THE DATABASE
  Future<void> fetchAndSetPlaces() async {
    final dataList = await DbHelper.getData(_tableName);
    _items = dataList
        .map(
          (item) => Place(
            id: item["id"],
            title: item["title"],
            image: File(item["image"]),
            // RETRIEVING THE IMAGE BASED ON PATH STORED IN DB
            location: PlaceLocation(
              latitude: item["loc_lat"],
              longitude: item["loc_lng"],
              address: item["address"],
            ),
          ),
        )
        .toList();

    notifyListeners();
  }

  Place findById(String id) {
    return _items.firstWhere((place) => place.id == id);
  }

}
